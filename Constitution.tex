\documentclass[10pt, letterpaper, onecolumn, oneside]{article}
\usepackage[utf8]{inputenc} % Deal with non-ASCII characters
\usepackage[margin=1in]{geometry} % Adjust margins
\usepackage{titlesec} % Custom headings
\usepackage[bottom]{footmisc} % Footnotes at bottom of page
\usepackage{hyperref} % Clickable contents
\usepackage{xstring} % Various logic for strings
\usepackage{enumitem} % Custom ordered lists
\usepackage{graphicx} % Embed images


% Hyperlink options
\hypersetup{
    breaklinks,
    colorlinks,
    linkcolor=red,
    urlcolor=blue,
    bookmarksopen,
    bookmarkstype=toc,
    pdftitle={ASULUG Constitution},
    pdfdisplaydoctitle
}

% Paragraph spacing
\linespread{1.1}
\setlength\parindent{0pt}
\setlength\parskip{0.8em}

% Disable hyphenation
\tolerance=1
\emergencystretch=\maxdimen
\hyphenpenalty=10000
\hbadness=10000

% Title formatting
\makeatletter
\renewcommand{\@maketitle}{
    \newpage
    \begin{center}
        {\LARGE \@title \par}
        \vspace*{0.8em}
        {\small Last Revised \@date}
    \end{center}
    \vspace*{1em}
    \par
}
\makeatother

% Article (section) headings
\renewcommand{\thesection}{Article~\Roman{section}}
\titleformat{\section}
    {\normalfont\Large\bfseries}
    {\thesection:~}
    {0pt}
    {}
\titlespacing{\section}{0pt}{0.3em}{-0.2em}

% Unified article environments
\newenvironment{article}[2]{
    \section[\thesection:~#1]{#1} \label{art:#2}
    \begin{sections}
}{
    \end{sections}
}
\newenvironment{article*}[2]{
    \phantomsection \addcontentsline{toc}{section}{#1}
    \section*{#1} \label{art:#2}
}{}

% Section lists
\newlist{sections}{enumerate}{2}
\setlist[sections, 1]{
    label={\bfseries\S{}\arabic*},
    ref={\S{}\arabic*},
    align=left,
    leftmargin=0.4in,
    labelwidth=0.25in,
    topsep=0pt
}
\setlist[sections, 2]{
    label={\bfseries\S{}\arabic{sectionsi}.\arabic*},
    ref={\S{}\arabic{sectionsi}.\arabic*},
    align=left,
    leftmargin=0.15in,
    labelwidth=0.4in,
    topsep=0pt
}

% Inline lists
\newlist{andlist}{enumerate*}{1}
\setlist[andlist]{
    label={(\alph*)},
    before=\unskip{:\ },
    itemjoin={;\ },
    itemjoin*={; and\ }
}
\newlist{orlist}{enumerate*}{1}
\setlist[orlist]{
    label={(\alph*)},
    before=\unskip{:\ },
    itemjoin={;\ },
    itemjoin*={; or\ }
}

% Signatures
\graphicspath{{signatures/}}
\newcommand{\signed}[3]{
    \includegraphics[width=2.2in,height=0.5in,keepaspectratio]{#2}
    & #3 \\
    \hline
    #1 \\[0.1in]
}

% Common phrases
\title{ASULUG Constitution 2021--2022}
\date{17~April~2021}
\newcommand{\name}{Linux Users Group at ASU}
\newcommand{\acronym}{ASULUG}



\begin{document}

    \maketitle

    \begin{article*}{Preamble}{preamble}

        We, the members of the \name, subscribing to the regulations and
        policies of Arizona State University, establish this Constitution to
        govern this Organization.

    \end{article*}

    \begin{article}{Name~and~Purpose}{name}

        \item The name of this Organization shall be the {\name}. Where
        appropriate, this Organization may also be called {\acronym}.

        \item The purpose of this Organization shall be to promote Free and
        Open-Source Software (FOSS) with a focus on Linux and other
        Unix-inspired operating systems.

    \end{article}

    \begin{article}{Communication~and~Engagement}{communication}

        \item This Organization operates three Primary Communication Channels
        \begin{andlist} \item a channelized chat and VoIP environment provided
        by Discord Inc. (``Discord Server") \item a
        LISTSERV\textregistered{}-based email list hosted by ASU (``Email List")
        \item ASU's SunDevilSync online student involvement system
        (``SunDevilSync") \end{andlist}. Access to all of these Primary
        Communication Channels, to the extent that users may subscribe to
        Organization announcements, shall be free and open to all individuals
        eligible to participate in Organization activities, including
        non-Members. Access instructions may be publicly posted, or eligible
        individuals may request up-to-date access instructions from any Officer
        at any time.

        \item The Discord Server and Email List shall also serve as Primary
        Engagement Platforms of the Organization. Access to engagement and
        interaction features shall be guaranteed to Members and provided to
        non-Members to the extent determined by the President and Vice
        President.

        \item The Officers shall also maintain Secondary Communication Channels
        and Secondary Engagement Platforms to the extent determined by the
        Officers as reasonable for improved Organization accessibility,
        Organization outreach, and/or otherwise advancing the Organization
        purpose. Secondary Communication Platforms and Secondary Engagement
        Platforms may include, but are not limited to, websites for content
        distribution or social media.

    \end{article}

    \begin{article}{Membership}{membership}

        \item There are three categories of Membership in this Organization
        \begin{andlist} \item Student Membership \item Alumni Membership \item
        Guest Membership \end{andlist}.

        \begin{sections}

            \item Student Membership shall be free and open to all students
            enrolled in at least one~(1) semester hour of credit at
            ASU.\footnote{In compliance with the
            \href{https://www.asu.edu/aad/manuals/ssm/ssm1302-01.html}{ASU
            Student Services Manual \S{}1302--01}.} Student Membership is
            granted to any student who attends two~(2) or more Organization
            meetings or events during a semester (see \ref{end:attendance}).
            Student Membership may also be granted to students who contribute
            regularly to discussion on any combination of Organization Primary
            Engagement Platforms (see \ref{end:regular}). Benefits of student
            membership shall include, but are not limited to \begin{andlist}
            \item access to Communication Platforms and Engagement Platforms of
            this Organization \item the right to attend Organization meetings
            and events \item the right to vote in Organization elections and on
            Organization matters \end{andlist}.

            \item Alumni Membership shall be granted to any ASU alumnus who once
            held a Student Membership. Guest Membership shall be granted to any
            non-student who is not eligible for Alumni Membership and who
            otherwise satisfies the requirements for Student Membership. Alumni
            Members and Guest Members shall not have the right to vote in
            Organization elections and on Organization matters, but shall
            otherwise receive the same benefits as Student Members.

        \end{sections}

        \item Any Member who is not an Officer may be removed at the discretion
        of the President, the Vice President, and any single Advisor, who shall
        unanimously agree upon removal. If unanimity is not reached, the Member
        shall not be removed. This section shall apply to any category of
        Membership and is intended to be invoked only rarely, i.e., for only the
        most egregious acts or behaviors which are detrimental to ASU, the
        \acronym{} brand, and/or the proper and expected functioning of the
        Organization.\footnote{Misconduct is discussed in the
        \href{https://www.asu.edu/aad/manuals/ssm/ssm1302-01.html}{ASU Student
        Services Manual \S{}1302--01}.} Removal of Officers is addressed
        separately in \ref{art:officers}.

    \end{article}

    \begin{article}{Officers~and~Elections}{officers}

        \item This Organization shall have four Officers \begin{andlist} \item
        President \item Vice President \item Finances Officer \item Public
        Relations Officer \end{andlist}.

        \begin{sections}

            \item The President is the public face of and primary point of
            contact for the Organization. The President may be expected to
            interact with non-Members in the course of Organization functions
            and as necessary for the promotion of the Organization and its
            activities. The President shall also be responsible for Organization
            administration, planning and coordinating Organization activities,
            and responding to inquiries from Members.

            \item The Vice President shall share the duties of the President, as
            directed by the President. Should the President become unable to
            fulfill their duties for an extended time, with or without advance
            notice, the Vice President shall fully assume the duties of the
            President during such period, and \ref{sec:officers:removal} may
            apply.

            \item The Finances Officer shall solicit and manage Organization
            funding, including the completion of budget requests and, where
            possible, oversight of Organization transactions. The Finances
            Officer shall also be responsible for seeking new funding sources as
            necessary and coordinating with the other Officers to ensure
            fulfillment of applicable funding stipulations.

            \item The Public Relations Officer shall administer the
            Organization's Communications Platforms and Engagement Platforms
            under the direction of the President and Vice President. The Public
            Relations Officer shall additionally be responsible for recruitment
            of new Members in coordination with the President, especially by
            promoting the Organization at ASU events and via other means
            provided by ASU.

            \item \label{sec:officers:president-delegation} Should other duties,
            not identified here, become necessary or warranted for the
            functioning of the Organization, the President shall be responsible
            for assuming such duties or delegating such duties to another
            Officer.

        \end{sections}

        \item At no point shall any Officer, speaking or acting on behalf of the
        Organization, represent or imply to represent ASU. Attempts at such
        representation may be grounds for removal under
        \ref{sec:officers:removal}.

        \item Officers shall have previously held an Organization Membership and
        shall be eligible Student Members for the duration of their
        terms.\footnote{Eligibility criteria is discussed in the
        \href{https://www.asu.edu/aad/manuals/ssm/ssm1302-01.html}{ASU Student
        Services Manual \S{}1302--01}.} No specific or explicit qualifications
        shall be required.

        \item \label{sec:officers:unique} No individual may hold more than one
        (1) Office simultaneously, i.e., all Officers must be unique.

        \item \label{sec:officers:elections} Officers shall be elected annually
        in the month of April. Student Members shall vote for each office
        independently, such that each arrangement of officers shall be valid
        under \ref{sec:officers:unique} and shall respect all candidates' Office
        preferences. That is, should a candidate be unwilling to accept a
        certain Office, they may not be voted into such Office. The President
        and Vice President shall execute the election in a manner reasonable and
        fair to all candidates, including a period preceding the election during
        which eligible individuals may nominate themselves as candidates for
        Office.

        \item \label{sec:officers:candidacy} Any Student Member may run for any
        office. A Student member may choose to run for multiple offices.

        \item \label{sec:officers:election-process} Student Members shall elect
        Officers through ranked-choice voting. For every office election,
        Student Members shall rank their candidate choices. Internally, the
        initial votes will be assigned to the Members' first choices. The
        candidate with the fewest first-choice votes shall be eliminated from
        the candidate pool for this office, and all first-choice ballots for
        this candidate shall be reassigned to their respective second-choice
        votes. This shall repeat until any candidate receives greater than than
        one-half of the total Student Member votes for that office; this
        candidate would then be declared as the winning nominee for that office.

        \item \label{sec:officers:election-multiple-offices} Each internal round
        of the election process under \ref{sec:officers:election-process} shall
        occur independently, but not concurrently, for each office. The internal
        selection round will proceed first for the office of the President,
        followed by the Vice President, Finances Officer and Public Relations
        Officer. If a candidate wins the nomination for any office, they shall
        be removed from the candidate pool of all subsequent selection rounds.

        \item \label{sec:officers:election-winners} The winning arrangement of
        Officers shall be that which receives more votes than all other valid
        arrangements. In the event of a tie for winning arrangement, a second
        election shall be held consisting of only the tied winning arrangements
        of the initial election. In the event of a second tie for winning
        arrangement, a random electronic draw shall be used to break the tie.

        \item Terms of Office shall begin on 15 May and conclude on 14 May of
        the following calendar year. Officers are eligible for re-election, and
        no limit shall exist on the number of terms for which an Officer may be
        elected.

        \item \label{sec:officers:vacancy} Should a vacancy in Office, except
        the Office of the President, arise with forty (40) or more days
        remaining in the term of Office, the President shall promptly execute an
        election in accordance with \ref{sec:officers:elections} and
        \ref{sec:officers:election-winners} to re-establish a complete board of
        Officers, except that all existing Officers shall retain their existing
        Offices. Should fewer than forty (40) days remain in the term, the
        duties of the vacant Office shall be assumed by the remaining Officers
        and delegated as in \ref{sec:officers:president-delegation}.

        \begin{sections}

            \item Should the Office of the President become vacant at any time,
            the Vice President shall immediately assume the Office of the
            President. The vacancy in the Office of the Vice President shall
            then be addressed in accordance with \ref{sec:officers:vacancy}.

        \end{sections}

        \item \label{sec:officers:removal} Any Officer may be removed from
        Office, at any time and for any reason, if all other Officers and any
        single Advisor unanimously support the removal. If unanimity cannot be
        reached, the agreement of any three (3) or more Student Members, who are
        not Officers, may be used in place of agreement from one (1) Officer.
        The removal of the President shall additionally require agreement from
        two (2) Student Members not holding Office, or, if the unanimity
        exception is invoked, a total of five (5) Student Members not holding
        Office. Any resultant vacancy shall be addressed in accordance with
        \ref{sec:officers:vacancy}.

        \item Should the Officers determine that an Organization function or
        subset of functions, including potentially one or more duties of Office,
        would be appropriately delegated to a Member who is not an Officer, such
        a delegation may be arranged with the permission of the President and of
        the delegated individual. Such delegations shall not outlast the term of
        the delegating Officers, except where the succeeding Officers renew the
        arrangement.

        \item In situations where the Organization entirely lacks a board of
        Officers, all Advisors shall be granted permission to \begin{orlist}
        \item initiate a vote for election of a board \item given a lack of a
        healthy Organization Membership, directly establish a board or bequeath
        this power to another Member of the Organization \end{orlist}.

    \end{article}

    \begin{article}{Meetings~and~Events}{meetings}

        \item Meetings and events shall be held on an ad hoc basis, depending on
        factors such as Membership interest and availability of hosts.

        \item Organization meetings and events shall be free and open whenever
        possible.

        \item The President and Vice President shall plan and organize meetings
        and events in collaboration with prospective presenters and meeting
        hosts. The President and Vice President may request assistance from
        other Officers and/or the Advisors.

        \item The Public Relations Officer shall be responsible for publicizing
        meetings and events on Primary Communication Channels of the
        Organization, at least five (5) days in advance when possible.

    \end{article}

    \begin{article}{Advisors}{advisors}

        \item There shall be one (1) or more faculty or staff Advisors for each
        academic year, who shall all be ex officio Members with no voting
        privileges.

        \item At least one Advisor shall be selected prior to the end of the
        preceding academic year by the Officers-elect. If this is not possible,
        an Advisor shall be selected as soon as possible thereafter. Additional
        Advisors may be selected at any time.

        \item Advisors shall assist the Officers with administrative matters and
        shall provide mentoring for Organization projects.

    \end{article}

    \begin{article}{Adoption~and~Amendments}{adoption}

        \item This Constitution shall be unanimously accepted, as soon as
        possible following annual elections, by all Officers-elect and all
        Advisors selected at that time by the Officers-elect. Thereafter, in
        coordination with the incumbent Officers, the Constitution shall be
        distributed via Primary Communication Channels for Student Members,
        including Officers and Officers-elect, to motion to adopt or reject the
        proposed Constitution. After no less than three (3) days, should there
        be more motions to adopt than motions to reject, the Constitution shall
        be considered ratified. If this condition is not satisfied, a new draft
        of the Constitution shall be proposed, and this process shall repeat.

        \item This Constitution, and no other Constitution, shall govern this
        Organization for the duration of the term of the Officers-elect who
        initially accepted this Constitution.

        \item At any time, any Member may propose an amendment to the
        Constitution by notifying the President. The amendment process shall
        begin only if and when the support of either three (3) Officers or two
        (2) Officers and any single Advisor is gained; otherwise the amendment
        shall be tabled for reconsideration in the succeeding Constitution. The
        amendment process shall be conducted similarly to the normal acceptance
        and adoption process --- the Officers, all Advisors, and the Member who
        proposed the Amendment shall unanimously accept a draft of the revised
        Constitution before releasing the draft for a Student Member vote.
        Seventy (70) percent or more of votes must favor adoption of the revised
        Constitution, in which case the revised Constitution shall be ratified
        and shall take effect no less than seven (7) days after the
        ratification. If a sufficient majority is not achieved, a new draft
        shall be proposed, and the voting process shall be repeated. If a
        sufficient majority is again not achieved, the amendment shall be tabled
        for reconsideration in the succeeding Constitution.

        \item This Constitution shall be a living document and shall carry the
        date on which it was last modified. The revision history of this
        Constitution, dating back at least to the originally adopted version
        with no amendments, shall be made available to all Members.

    \end{article}

    \begin{article*}{Non-Discrimination~Statement}{non-discrimination}

        Membership and all privileges, including voting and officer positions,
        must be extended to all students without regard to age, ethnicity,
        gender, disability, color, national origin, race, religion, sexual
        orientation, or veteran status. Title~IX of the Educational Amendments
        of 1972, \S{}106.14, makes an exception for social fraternities and
        sororities, in regard to gender, for membership criteria. Religious
        student organizations will not be denied registration solely because
        they limit membership or leadership positions to students who share the
        same religious beliefs. These groups, however, may not discriminate in
        membership or leadership on any other prohibited basis (i.e., age,
        ethnicity, gender, disability, color, national origin, race, sexual
        orientation, or veteran status).

    \end{article*}

    \begin{article*}{Endnotes}{endnotes}
        \begin{sections}[label={\arabic*.}, ref={Endnote~\arabic*}]

            \item \label{end:attendance} An individual shall be considered to
            have attended a meeting or event if that individual was present at
            any site of the meeting or event, including any applicable Primary
            Engagement Platforms or Secondary Engagement Platforms, for one (1)
            hour or fifty (50) percent of the duration of the meeting or event,
            whichever is less. If the duration of presence cannot reasonably be
            determined or estimated, individuals shall receive the benefit of
            the doubt and may self-report their attendance without dispute.

            \item \label{end:regular} The President, entertaining
            recommendations from other Officers, shall have ultimate authority
            to issue the final determination of regular contribution. In
            general, multiple ``meaningful" messages per day over a period of
            twenty (20) days, not necessarily contiguous, shall serve as a
            baseline for this determination. Any individual seeking Membership
            via this route is responsible for requesting activity review by the
            President; the process shall not be automatic.

        \end{sections}
    \end{article*}

    \vfill

    \begin{tabular}{p{2.5in} @{\hspace{0.2in}} p{1in}}
        \signed{Ethan~Plumb,~President}{}{}
        \signed{Ryan~Slawson,~Vice~President}{}{}
        \signed{Ryan~Shaver,~Finances~Officer}{}{}
		\signed{Devansh~Patel,~Public~Relations~Officer}{}{}
        \signed{Kevin~Burger,~Faculty~Advisor}{}{}
        \signed{Ryan~Meuth,~Faculty~Advisor}{}{}
    \end{tabular}

\end{document}
